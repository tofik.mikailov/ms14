package az.ingress.ms14;

import az.ingress.ms14.model.PhoneNumber;
import az.ingress.ms14.model.Student;
import az.ingress.ms14.repository.PhoneNumberRepository;
import az.ingress.ms14.repository.StudentRepository;
import az.ingress.ms14.service.StudentService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
@RequiredArgsConstructor
public class Ms14Application implements CommandLineRunner {

    private final StudentService studentService;
    private final PhoneNumberRepository phoneNumberRepository;
    private final StudentRepository studentRepository;

    public static void main(String[] args) {
        SpringApplication.run(Ms14Application.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        Student student = studentRepository.findById(2).get();
        System.out.println(student.getPhoneNumberList());

//        PhoneNumber phone1 = PhoneNumber.builder()
//                .number("+994505005050")
//                .student(student)
//                .build();
//        PhoneNumber phone2 = PhoneNumber.builder()
//                .number("+994506785432")
//                .student(student)
//                .build();
//        phoneNumberRepository.save(phone1);
//        phoneNumberRepository.save(phone2);
//
//        student.getPhoneNumberList().add(phone1);
//        student.getPhoneNumberList().add(phone2);
//        studentRepository.save(student);



//        PhoneNumber phone3 = PhoneNumber.builder()
//                .number("+994502222222")
//                .build();
//        phoneNumberRepository.save(phone3);


        //JDBC
//        Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5455/postgres","postgres","password");
//        Statement statement = con.createStatement();
//        ResultSet resultSet = statement.executeQuery("select * from student");
//        while (resultSet.next()) {
//            System.out.println(resultSet.getInt(1));
//            System.out.println(resultSet.getInt(2));
//            System.out.println(resultSet.getString(3));
//        }


//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenceUnitName");
//        EntityManager entityManager = emf.createEntityManager();
//        entityManager.getTransaction().begin();
//
//        List<Student> resultList = entityManager.createNativeQuery("select * from student", Student.class)
//                .getResultList();
//        System.out.println(resultList);
//
//        entityManager.getTransaction().commit();
//        entityManager.close();
    }
}
