package az.ingress.ms14.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
//CRUD
public class StudentController {

    @GetMapping
    public String greeting() {
        return "Hello from Ms 14";
    }

}
