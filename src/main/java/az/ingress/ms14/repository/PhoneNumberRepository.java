package az.ingress.ms14.repository;

import az.ingress.ms14.model.PhoneNumber;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Long> {

//    List<PhoneNumber> findAllByStudent_Id(Integer id);
}
