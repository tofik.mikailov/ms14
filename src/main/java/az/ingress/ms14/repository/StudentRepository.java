package az.ingress.ms14.repository;

import az.ingress.ms14.model.Student;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface StudentRepository extends JpaRepository<Student, Integer> {

    List<Student> findAllByAgeGreaterThan(int age);

    List<Student> findAllByPasswordLike(String password);

    @Query(value = "select * from student where name = :a", nativeQuery = true)
    Optional<Student> findByNameWhereNameEquals(String a);

    List<Student> findAllByNameStartsWith(String prefix);
}
