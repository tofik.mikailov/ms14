package az.ingress.ms14.service;

import az.ingress.ms14.model.Student;
import az.ingress.ms14.repository.StudentRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;
    private final EntityManagerFactory emf;

    public Student save(Student student) {
        return studentRepository.save(student);
    }

    public Student manualSave(Student student) {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        Student entity = entityManager.merge(student);
        entityManager.getTransaction().commit();
        entityManager.close();
        return entity;
    }

    public List<Student> getStudentsAgeMoreThan(int age) {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();

        List<Student> result = entityManager.createNativeQuery("select * from student where age > :age", Student.class)
                .setParameter("age", age).getResultList();

        entityManager.getTransaction().commit();
        entityManager.close();
        return result;
    }

    public List<Student> getStudentsAgeMoreThanRepository(int age) {
        return studentRepository.findAllByAgeGreaterThan(age);
    }

    public List<Student> getStudentPasswordLike(String password) {
        return studentRepository.findAllByPasswordLike(password);
    }

    public Optional<Student> getByNAme(String name) {
        return studentRepository.findByNameWhereNameEquals(name);
    }
}
