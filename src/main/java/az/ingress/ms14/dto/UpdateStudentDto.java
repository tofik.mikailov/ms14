package az.ingress.ms14.dto;

import lombok.Data;

@Data
public class UpdateStudentDto {

    Integer id;
    String name;
    String surname;
}
