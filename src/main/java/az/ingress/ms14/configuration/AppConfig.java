package az.ingress.ms14.configuration;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("ms")
@Data
public class AppConfig {

    private String name;
    private String teacher;
    private List<String> students;
}
